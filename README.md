# Dark Client Library

Use this package to access Dark Backend services.

## Example

```javascript
const ApiClient = require('dark-client')

async function testApiClient() {
  try {
    console.log(`>> ENV >> ${process.env.NODE_ENV}`)

    let res
    const api = new ApiClient({ url: 'some-api-url', username: 'some-user', password: 'some-password'})

    console.log(`>> STORE >>`)
    res = await api.get( 'store', '59c042222a985ae70278619f')
    console.log(res[0])

    console.log(`>> USER >>`)
    res = await api.query( 'user', { limit: 10 })
    res.forEach(u => {
      console.log(u)
    })
  } catch(err) {
    console.log(`>> err.message >> ${err.message}`)
    console.log(`>> err.stack >> ${err.stack}`)
    console.log(`>> err.data >> ${err.data ? JSON.stringify(err.data) : 'No Data'}`)
  }
}

testApiClient()

/*---------------------------------------------------------------------------------------------*/

const ServiceClient = require('dark-client')

async function testServiceClient() {
  try {
    console.log(`>> ENV >> ${process.env.NODE_ENV}`)

    let res
    const service = new ServiceClient({ authUrl: 'some-auth-url', url: 'some-service-url', apikey: 'some-key', headers: {...} })

    console.log(`>> SEND EMAIL >>`)
    res = await service.request({ entity: 'some-entity', route: 'some-route', body: {...} })
    console.log(res)

  } catch(err) {
    console.log(`>> err.message >> ${err.message}`)
    console.log(`>> err.stack >> ${err.stack}`)
    console.log(`>> err.data >> ${err.data ? JSON.stringify(err.data) : 'No Data'}`)
  }
}

testServiceClient()
```
