const axios = require('axios');
const nanoid = require('nanoid/generate');
const uuid = require('uuid/v4');

const uniqueId = l => {
  // crypto.createHash('md5').update(uuid()).digest('hex')
  return nanoid('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', l);
};

const checkTypes = (v, t) => {
  return t.indexOf(typeof v) !== -1;
};

module.exports = class DarkClient {
  //
  constructor({
    authUrl = '/user/auth',
    url,
    apikey,
    username,
    password,
    hardwareId,
    token,
    headers,
  }) {
    this.authUrl = authUrl;
    this.baseUrl = url;
    this.apikey = apikey;
    this.username = username;
    this.password = password;
    this.hardwareId = hardwareId;
    this.authenticated = false;
    this.accountAuthentication = false;
    this.headers = headers;
    this.session = {
      token,
    };

    // inject did
    this.axios = req => {
      const { headers = {}, did } = req;
      headers['x-darkpos-did'] = did || uuid();
      req.headers = headers;
      return this._axios(req);
    };

    this._axios = axios.create({
      baseURL: this.baseUrl,
    });

    // default headers

    this._axios.defaults.headers.common['Content-Type'] = 'application/json';

    if (this.headers) {
      Object.keys(this.headers).forEach(key => {
        this._axios.defaults.headers.common[key.toLowerCase()] = this.headers[key];
      });
    }

    // signature auth
    if (this._axios.defaults.headers.common['x-darkpos-api-signature']) {
      this.authenticated = true;
    }

    // token auth
    if (this.session.token) {
      this._axios.defaults.headers.common.authorization = `Bearer ${this.session.token}`;
    }
    if (this._axios.defaults.headers.common.authorization) {
      this.authenticated = true;
      this.session.token = this._axios.defaults.headers.common.authorization.replace('Bearer ', '');
    }

    // account auth
    if (this.apikey || (this.username && this.password)) {
      this.accountAuthentication = true;
    }

    return this;
  }

  //
  getToken() {
    return this.session.token;
  }

  //
  isAuthenticated() {
    return this.authenticated;
  }

  //
  async createApiUser(data, did) {
    const _data = Object.assign(data, {
      username: data.username || `api_${uniqueId(32)}`,
      password: data.password || `key_${uniqueId(32)}`,
      attributes: ['USER_IS_LOGIN', 'USER_IS_ACTIVE', 'ROLE_API'],
      _accountId: `${data.account}`,
      _storeId: `${data.stores[0]}`,
    });
    return Object.assign(await this.create('user', _data, did), { password: _data.password });
  }

  //
  async createAdminUser(data, did) {
    const _data = Object.assign(data, {
      username: data.username || `adm_${uniqueId(32)}`,
      password: data.password || `key_${uniqueId(32)}`,
      attributes: ['USER_IS_LOGIN', 'USER_IS_ACTIVE', 'ROLE_ADMIN'],
    });
    return Object.assign(await this.create('user', _data, did), { password: _data.password });
  }

  //
  async create(entity, data, did) {
    return this.post(entity, data, did);
  }

  // query = {}, sort = {}, limit = 10, skip = 0
  async query(entity, data, did) {
    return this.post(`${entity}/query`, data, did);
  }

  //
  async meta(entity, data, did) {
    return this.post(`${entity}/meta`, data, did);
  }

  //
  async post(entity = '', data, did) {
    return this.request({
      method: 'POST',
      url: `/${entity.toLowerCase()}`,
      data,
      did,
    });
  }

  //
  async get(entity = '', id, did) {
    return this.request({
      method: 'GET',
      url: `/${entity.toLowerCase()}${id && checkTypes(id, ['number', 'string']) ? `/${id}` : ''}`,
      did,
    });
  }

  //
  async update(entity, id, data, did) {
    return this.put(entity, id, data, did);
  }

  //
  async put(entity = '', id, data, did) {
    return this.request({
      method: 'PUT',
      url: `/${entity.toLowerCase()}/${id}`,
      data,
      did,
    });
  }

  //
  async delete(entity = '', id, did) {
    return this.request({
      method: 'DELETE',
      url: `/${entity.toLowerCase()}/${id}`,
      did,
    });
  }

  //
  async request({
    method = 'POST',
    url = '',
    entity = '',
    route = '',
    data,
    did = uuid(),
    reset = false,
    timeout = 0,
  }) {
    if (reset) this.authenticated = false;
    url =
      url ||
      (entity ? `/${entity.toLowerCase()}/${route.toLowerCase()}` : `/${route.toLowerCase()}`);

    try {
      const res = await (await this.auth(did)).axios({
        method,
        url,
        data,
        did,
        timeout,
      });

      if (res.data.status === 'success') return res.data.data;

      const err = new Error((res.data.data && res.data.data.message) || 'unknown error occurred');
      throw Object.assign(err, { data: res.data.data || {} });
    } catch (err) {
      const { statusCode, data: error = {} } = (err.response && err.response.data) || {};
      if (statusCode === 401) {
        if (this.authenticated && this.accountAuthentication) {
          this.authenticated = false;
          return this.request({ method, url, data, did, reset: true, timeout });
        }
        throw Object.assign(new Error(error.message || 'Unauthorized'), { data: error });
      }
      throw err;
    }
  }

  //
  async auth(did) {
    if (this.authenticated || !this.accountAuthentication) return this;

    const res = await this.axios({
      method: 'POST',
      url: this.authUrl,
      data: {
        apikey: this.apikey,
        username: this.username,
        password: this.password,
        hardwareId: this.hardwareId,
      },
      did,
    });

    this.authenticated = true;
    this.session = res.data.data;
    this._axios.defaults.headers.common.authorization = `Bearer ${this.session.token}`;

    return this;
  }

  //
  authCheck(token, did) {
    return this.axios({
      method: 'GET',
      url: `${this.authUrl}/check`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      did,
    });
  }
};
